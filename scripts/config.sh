#!/bin/bash

export MASTER_IP="192.168.5.101"
export MASTER_HOSTNAME="mesos-master"

export SLAVE_IP="192.168.5.102"
export SLAVE_HOSTNAME="mesos-slave"
