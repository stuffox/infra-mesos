#!/bin/bash

source /vagrant/scripts/config.sh

# --- Docker repository --
apt-get install -y apt-transport-https ca-certificates

apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | tee /etc/apt/sources.list.d/docker.list

apt-get update

apt-get install -y linux-image-extra-$(uname -r)
apt-get install -y linux-image-extra-virtual

# --- Installing Docker ---
apt-get install -y docker-engine

# --- Slave ---
stop zookeeper
echo "manual" | tee /etc/init/zookeeper.override

stop mesos-master
echo "manual" | tee /etc/init/mesos-master.override

echo "${SLAVE_IP}" | tee /etc/mesos-slave/ip
echo "${SLAVE_HOSTNAME}" | tee /etc/mesos-slave/hostname

echo 'docker,mesos' | tee /etc/mesos-slave/containerizers
echo '5mins' | tee /etc/mesos-slave/executor_registration_timeout

service mesos-slave restart
