#!/bin/bash

source /vagrant/scripts/config.sh

apt-get install -y marathon

stop mesos-slave
echo manual | tee /etc/init/mesos-slave.override

# --- Configure Zookeeper ---
echo 1 | tee /etc/zookeeper/conf/myid

sed -i s/#server\.1=zookeeper1:2888:3888/server.1=${MASTER_IP}:2888:3888/ /etc/zookeeper/conf/zoo.cfg

# --- Configure Master ---
echo 1 | tee /etc/mesos-master/quorum
echo "${MASTER_IP}" | tee /etc/mesos-master/ip
echo "${MASTER_IP}" | tee /etc/mesos-master/hostname

# --- Configure Marathon ---
mkdir -p /etc/marathon/conf
echo "zk://${MASTER_IP}:2181/mesos" | tee /etc/marathon/conf/master
echo "zk://${MASTER_IP}:2181/marathon" | tee /etc/marathon/conf/zk
echo "${MASTER_HOSTNAME}" | tee /etc/marathon/conf/hostname

# --- Services ---
service zookeeper restart
service mesos-master restart
service marathon restart
